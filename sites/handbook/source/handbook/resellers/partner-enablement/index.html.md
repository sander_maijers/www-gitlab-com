---
layout: handbook-page-toc
title: "Channel Partner Technical Presales Enablement"
---


## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}
<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />
{::options parse_block_html="true" /}


In this section of the Channel Partner Programs Handbook we review assets and enablement from the perspective of GitLab Partners' Technical constituencies.

Beyond our Channel Partners Handbook pages, you will find sales guides, use cases, training materials, and program guides reviewed below and hosted and updated in our [GitLab Partner Portal](https://partners.gitlab.com/). The materials should be a great place to start effectively selling, serving and hitting your number with GitLab.

Rember that some of the links below require you to login to [GitLab's Partner Portal](https://partners.gitlab.com/) first. If you haven't already, [here is where you register for portal access](https://partners.gitlab.com/English/).

# Basic Presales Knowledge
{: .gitlab-orange}

## Get to know GitLab, what it is, what it offers, and what it does

1.  **[GitLab internal Sales Training Resources](https://about.gitlab.com/handbook/sales/training/)** - Review this page for all of the sales related training that internal GitLab sellers go through.

1.  **[GitLab Promo Video](https://about.gitlab.com/why-gitlab/)** - This is the GitLab Trailer.  It's actually a good elevator pitch!  Check it out. (1 min)

1.  **[GitLab Promo DEMO Video](https://about.gitlab.com/demo/)** - This video is a bit longer demo video and shows more about using GitLab (3 min)

1.  **[Platform Page](https://about.gitlab.com/platform)** - Get to know the basic capability set of GitLab here.  Be sure to scroll down to the Solutions GitLab enables and drill into them for more information (20 min)

1.  **[Solutions Page](https://about.gitlab.com/solutions/)** - offers a wide variety of ways customers leverage GitLab (20 min)


## Pitch GitLab and uncover new opportunities

1.  **[Building Pipelines GitLab Partner Webinar Series](https://content.gitlab.com/viewer/63bddf02edadd0b1346a73db)** - This webinar series is exclusively for GitLab Partners.  We discuss various sales and presales-level topics that help you build your sales pipeline with GitLab.  Register Today! (5 min)

1.  **Getting Started with GitLab:**
    - **[Get Started with GitLab for Small Business](https://about.gitlab.com/get-started/small-business/)** - Links to the primary things SMB / Commercial customers need to do to get up and running successfully with GitLab
    - **[Get Started with GitLab for Enterprise](https://about.gitlab.com/get-started/enterprise/)** - Links to the primary things Enterprise customers need to do to get up and running successfully with GitLab
    - **[Build a Business Case for GitLab](https://about.gitlab.com/get-started/build-business-case/)** - Guidance for building a business case for GitLab DevSecOps Platform for a customer buying decision maker
    - **[Why GitLab](https://about.gitlab.com/why-gitlab/)** - Get to know the top reasons customers choose GitLab
    - **[Base Getting Started with GitLab Page](https://about.gitlab.com/get-started/)** - Review the rest of the assets on the Getting Started with GitLab page.  

1.  **[Positioning GitLab - Handbook Page](https://about.gitlab.com/handbook/positioning-faq/)** - Review this page to learn about how to position GitLab in the market. (10 min)

1.  **[GitLab Technical Discovery Guide](https://about.gitlab.com/handbook/sales/qualification-questions/#questions-for-selling-gitlab-premium-and-ultimate)** - Discovery is a crucial skill for any Solutions Architect (SA).  Here is how GitLab SA’s do their Discovery (10 min)

1.  **[Customer Case Studies](https://about.gitlab.com/customers/)** - Our case studies will give you ideas of why enterprises are choosing GitLab.  Bookmark this for customer justification in deals later. (5 min)

1.  **[User Personas](https://about.gitlab.com/handbook/product/personas/) vs. [Buyer Personas](https://about.gitlab.com/handbook/marketing/brand-and-product-marketing/product-and-solution-marketing/roles-personas/buyer-persona/)** - There are different personas to understand when selling a Platform like GitLab.  Which do you value more? (20 min)


## Learn how to solution and transact a GitLab deal

1.  **[Reference Architectures](https://docs.gitlab.com/ee/administration/reference_architectures/) and [Install Guides](https://docs.gitlab.com/ee/install/)** - GitLab has a leading market share in self-managed deployments.  Here are our technical specs and docs for solutioning and installing GitLab instances.  

1.  **[Start a Free Trial](https://gitlab.com/-/trials/new)** - Here is a link to start a free trial of GitLab SaaS on GitLab.com. This is an increasingly popular option. Plese refer to [this page of the Partner Portal](https://partners.gitlab.com/prm/English/c/marketing-free-trial) for details about how to set up a customized affiliate link to tag new trials to your partnership.

1.  **[List Price Page](https://about.gitlab.com/pricing/)** - This is the public price of the subscription.  Pricing is a simple per-user subscription that is the same whether you are using SaaS or Self Hosted instance. 

1. **[Partner Program Incentives Guide](https://partners.gitlab.com/prm/English/s/assets?id=350001)** - Review for more details on partner transactions.

1.  **[Online ROI calculators;](https://about.gitlab.com/calculator/)** - Leverage the online ROI calculators, or contact your Partner SA for more sophisticated analysis tools.


## Services opportunities when selling Gitlab

1.  **[Service Kits](https://partners.gitlab.com/prm/English/c/Channel_Service_Packages)** - We have developed service kits for you including sample Statements of Work (SOWs) and Levl of Effort (LOEs).

1.  **[Delivery Kits](https://gitlab.com/gitlab-org/professional-services-automation/delivery-kits)** - These delivery kits are what our internal PS teams use for customer projects.  Go grab what you need!


# Advanced Presales & Technical Knowledge
{: .gitlab-orange}

## Compete

1. **[GitLab Maturity Page](https://about.gitlab.com/direction/maturity/)** - A very transparent self-assessment of the maturity of each GitLab feature. No direct comparisons with other vendors here.

1. **[GitLab Customer Success Stories](https://about.gitlab.com/customers/)** - Our success stories, including customer testimonials.

1.  **[GitLab Competitive Overviews](https://partners.gitlab.com/prm/English/s/assets?collectionId=56958)** - Have a look at competitive positioning material in the assets library of the Partner site (login required).

## Technical

1. **[GitLab Documentation](https://docs.gitlab.com/)** - The best starting point for technical questions. It lists feature descriptions, excellent tutorials, and troubleshooting tips.

1. **[GitLab Blog](https://about.gitlab.com/blog/)** - Various content about GitLab. Mostly technical stuff, but not exclusively. Subscribing to the newsletter can be useful.

1. **[Release Overview Website](https://gitlab-com.gitlab.io/cs-tools/gitlab-cs-tools/what-is-new-since/?tab=features)** - A fairly unknown tool that can help with understanding the changes in each GitLab version. (Source code of the website is [here](https://gitlab.com/gitlab-com/cs-tools/gitlab-cs-tools/what-is-new-since).) Important features of this website to pay extra attention to:

    * [CVEs by version](https://gitlab-com.gitlab.io/cs-tools/gitlab-cs-tools/what-is-new-since/?tab=cves): easily keep track of security vulnerabilities in each GitLab version.

    * [Upgrade path](https://gitlab-com.gitlab.io/support/toolbox/upgrade-path/): a handy tool which can list the steps of upgrading self-managed GitLab from version X to version Y.

1.  **[Deprecations by version](https://docs.gitlab.com/ee/update/deprecations.html)** - same data as [this page](https://gitlab-com.gitlab.io/cs-tools/gitlab-cs-tools/what-is-new-since/?tab=deprecations) of the Release Overview website, just in a different format.

1. **[GitLab Releases Blog](https://about.gitlab.com/releases/categories/releases/)** - The same information as the Release Overview website, but in nicely formatted blog posts, ready to share with customers.

1. **[GitLab's Repository](https://gitlab.com/gitlab-org/gitlab)** - The source code of the GitLab software itself. The most useful part is the [Issues page](https://gitlab.com/gitlab-org/gitlab/-/issues), where everybody can check the state of a feature or a bug and see if it's planned or being worked on.


# Running Effective GitLab Demos and POCs
{: .gitlab-orange}

## Demo resources to get started showcasing the Platform

1. **[GitLab Partner Demo Delivery Guide](https://gitlab.com/gitlab-partner-demos/delivery-guide)** - a repository containing all the resources for partners to successfully deliver a 1-hour-long, high-level, technical overview demo of GitLab.

1.  **[Building Pipelines Episode 011](https://content.gitlab.com/viewer/63bddf02edadd0b1346a73db)** - “Effective GitLab Demos.” (40:16)  More resources and helpful hints are provided.  Register for the Webinar!

1. **[The GitLab Demo Handbook Page.](https://about.gitlab.com/handbook/customer-success/solutions-architects/demonstrations/)**- Its a good place to start for general purpose demos the same way GitLab SA's do it.

1.  **[GitLab Product Marketing](https://about.gitlab.com/handbook/marketing/brand-and-product-marketing/product-and-solution-marketing/#key-demo-videos)** has some product demo videos on their handbook page.


## Existing demo projects and guides you can leverage

1.  **[Ultimate GitOps Workshop](https://guided-explorations.gitlab.io/workshops/gitlab-for-eks/)** - Leverage GitLab K8s agent to deploy to EKS.

1.  **[GitLab Learn Labs](https://gitlab.com/gitlab-learn-labs/webinars/how-to-use-these-projects)** - GitLab initiative to create demos you can leverage

1.  **[Guided Explorations](https://gitlab.com/guided-explorations)** - Contains Joint Reference Architectures, Patterns, and Working Examples for integrating GitLab with Alliance and Technology solutions.

1.  **[Customer Success Workshops](https://gitlab.com/gitlab-com/customer-success/workshops/templates)** - A working group containing all CS Workshop Templates.

1.  **[SA Demo Catalog](https://gitlab.com/gitlab-com/customer-success/solutions-architecture/demo-catalog)** - Demo Archive of SA Demo's which can be reused or repurposed for your demos

1. **[CI Samples Demo](https://gitlab.com/gitlab-learn-labs/webinars/cicd/cicd-samples)** -  CI functions samples for quick demos

## Proof of Value (POV), aka POC’s


**[GitLab POV Handbook Page](https://about.gitlab.com/handbook/customer-success/solutions-architects/tools-and-resources/pov/)** - This handbook page is a great resource and link aggregation point for resources to conduct POVs


# Additional Resources
{: .gitlab-orange}

## Official GitLab Enablement on Level Up
1. [Access GitLab Training from the Partner Portal](https://partners.gitlab.com/prm/English/c/Training) - We use LevelUp that is accessible publicly, but please do not use the site directly as a Partner.  Please login through the Partner Portal and access training there.  This will ensure that the training you take will be associated with your company partnership with GitLab!

    ![Start Training from the Partner Portal](images/gitlab-partner-training.png){: .shadow}*Start Training from the Partner Portal*


## Additional Enablement Resources at GitLab

1. **[GitLab Unfiltered Partner Enablement Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KqQi2Kk0M5vnqB1n0CIb_sa)** - This is an SA curated playlist of the most informative technical videos on GitLab Unfiltered channel.  Some videos here are unlisted and hard to discover otherwise.  Bookmark this link!

1. **[GitLab Tutorials](https://docs.gitlab.com/ee/tutorials/)** - These tutorials are included in the GitLab Docs site.  They are a great starting point for technical capabilities.

1. **[GitLab Security Capabilities](https://www.youtube.com/watch?v=d8X6vYY4WOA)** - Also periodically browse the main GitLab Unfiltered channel.  This video tour of GitLab Security capabilities is excellent, but for technical reasons can't be added to the playlist above.  (50:16)

1.  [Partner Enablement Presentation](https://docs.google.com/presentation/d/1VUY6WR4gewnXITUUAnVbFOExXmhdTYif2JgAiTbT7tE/edit#slide=id.g12b319f6181_0_10) - This deck has some great links associa

## Third-Party Tutorials

1.  **GitLab for Beginners:**
    - [Learn GitLab in 3 Hours GitLab Complete Tutorial For Beginners](https://www.youtube.com/watch?v=8aV5AxJrHDg) (~ 3 hrs)
1.  **GitLab Flow:**
    - [GitLab Flow - GitLab Tutorial - Part I](https://www.youtube.com/watch?v=ZJuUz5jWb44) (15:37)
    - [GitLab Flow - GitLab Tutorial - Part II](https://www.youtube.com/watch?v=K5Ux_m0ccuo&list=RDCMUCxu8xrbPZ-neg3FhMdz4yrA&index=3) (33:00)
1.  **GitLab CI/CD:**
    - [GitLab CI/CD - GitLab Runner Introduction - 2022](https://www.youtube.com/watch?v=-CyVpfDQAG0&list=RDCMUCxu8xrbPZ-neg3FhMdz4yrA&index=6)(23:27)
    - [GitLab CI CD - Install and Configure GitLab Runner on Kubernetes with Helm](https://www.youtube.com/watch?v=0Fes86qtBSc) (29:41)

